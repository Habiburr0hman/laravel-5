<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Cast;

class CastController extends Controller
{
  public function __construct()
  {
    // $this->middleware('auth')->except('index');
    $this->middleware('auth')->only('create', 'store', 'edit', 'update', 'delete');
  }

  public function index()
  {
    // $cast = DB::table('casts')->get();
    $cast = Cast::all();

    return view('cast.index', compact('cast'));
  }

  public function create()
  {
    return view('cast.create');
  }

  public function store(Request $request)
  {
    $request->validate([
      'nama' => 'required|unique:casts',
      'umur' => 'required',
      'bio' => 'required'
    ]);
    // $query = DB::table('casts')->insert([
    //   "nama" => $request["nama"],
    //   "umur" => $request["umur"],
    //   "bio" => $request["bio"]
    // ]);

    // $cast = new Cast;
    // $cast->nama = $request["nama"];
    // $cast->umur = $request["umur"];
    // $cast->bio = $request["bio"];
    // $cast->save();

    $cast = Cast::create([
      "nama" => $request["nama"],
      "umur" => $request["umur"],
      "bio" => $request["bio"]
    ]);

    return redirect('/cast');
  }

  public function show($id)
  {
    // $cast = DB::table('casts')->where('id', $id)->first();
    $cast = Cast::find($id);

    return view('cast.show', compact('cast'));
  }


  public function edit($id)
  {
    // $cast = DB::table('casts')->where('id', $id)->first();
    $cast = Cast::find($id);

    return view('cast.edit', compact('cast'));
  }

  public function update($id, Request $request)
  {
    $request->validate([
      'nama' => 'required',
      'umur' => 'required',
      'bio' => 'required'
    ]);

    // $query = DB::table('casts')
    //   ->where('id', $id)
    //   ->update([
    //     'nama' => $request["nama"],
    //     'umur' => $request["umur"],
    //     'bio' => $request["bio"]
    //   ]);

    $update = Cast::where('id', $id)->update([
      "nama" => $request["nama"],
      "umur" => $request["umur"],
      "bio" => $request["bio"]
    ]);

    return redirect('/cast');
  }

  public function destroy($id)
  {
    // $query = DB::table('casts')->where('id', $id)->delete();
    Cast::destroy($id);

    return redirect('/cast');
  }
}
