<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  // protected $table = 'casts';
  // protected $fillable = ["nama", "umur", "bio"];
  protected $guarded = [];
}
